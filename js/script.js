function makeTable () {
    let table = document.createElement('table');
    let tableRows = [], tableCells = "";
    for (let i=0; i<30; i++) {
        for (let x=0; x<30; x++) {
         tableCells += "<td></td>";
        }
        tableRows[i] =`<tr>${tableCells}</tr>`;
        tableCells = "";
    }
    table.innerHTML = tableRows.join("");
    document.body.appendChild(table);
    return table;
}
makeTable();

document.body.addEventListener('click', function(){
      let el = event.target, cells = document.getElementsByTagName('td');
     if (el.tagName == "TD") {
    if (el.className === 'white' || el.className === '') {el.className = 'black';}
        else el.className = 'white';
    }
     if (el.tagName != "TD" && el.tagName != "TABLE") {
         for (let i=0; i <cells.length; i++)
         {if (cells[i].className === 'white' || cells[i].className === '') {cells[i].className = 'black';}
        else cells[i].className = 'white';
         }
     }
});